package iut.projet.pointk.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Collection;

import iut.projet.pointk.ExternalDB.ConnectMySql;
import iut.projet.pointk.InternalDB.DatabaseHelper;
import iut.projet.pointk.R;
import iut.projet.pointk.data.Product;

public class MainActivity extends AppCompatActivity {

    private static final String     TAG = "MainActivity";
    private Button                  mLoginPageBtn;
    private Context                 mContext;
    private DatabaseHelper mDBHelper;
    public static int userID;
    public static String LOGIN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initInternDB();
        Object[] account = mDBHelper.getAccount();
        if (account == null){
            Intent intent = new Intent(this, LoginPage.class);
            startActivity(intent);
        }
        else {
            MainActivity.userID = (int) account[0];
            MainActivity.LOGIN = (String) account[1];
            addProductListFromExternalDB();

            Intent intent = new Intent(this, HistoryActivity.class);
            startActivity(intent);

            //initView();
        }
        finish();
    }

    private void initView() {
        setContentView(R.layout.activity_main);
    }

    private void initInternDB() {
        mDBHelper=new DatabaseHelper(this.getApplicationContext());
    }

    private void addProductListFromExternalDB() {
        ConnectMySql.ConnectMySqlListener listener = new ConnectMySql.ConnectMySqlListener() {
            @Override
            public void onPostResult(Object[] result) {
                onGetResultAllProduct(result);
            }

            @Override
            public void onProgress(Void... values) {

            }
        };
        ConnectMySql connectMySql = new ConnectMySql( ConnectMySql.Request.ALL_PRODUCT_TYPE, listener);
        connectMySql.execute();
    }

    private void onGetResultAllProduct(Object[] result) {
        ArrayList<Product> allProduct = new ArrayList<Product>();
        for (Object o : result) {
            allProduct.add((Product) o);
        }

        Product.setAllProducts(allProduct);
    }
}
