package iut.projet.pointk.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import iut.projet.pointk.R;
import iut.projet.pointk.data.Product;
import iut.projet.pointk.view.NewPurchase;

public class ItemPurchaseAdapter extends ArrayAdapter<Product> {
    private final NewPurchase.OnClickProductItem mOnClickProductItem;
    private Context     mContext;
    private int         mListItemLayout;

    public ItemPurchaseAdapter(Context ctx, int layoutId, ArrayList<Product> products, NewPurchase.OnClickProductItem onClickProductItem) {
        super(ctx, layoutId, products);

        this.mContext = ctx;
        this.mListItemLayout = layoutId;
        this.mOnClickProductItem = onClickProductItem;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Product product = getItem(position);

        final ItemPurchaseAdapter.ViewHolder viewHolder;
        if(convertView == null) {
            viewHolder = new ItemPurchaseAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mListItemLayout, parent, false);

            viewHolder.mIcon = (ImageView) convertView.findViewById(R.id.icon);
            viewHolder.mTypeTxt = (TextView) convertView.findViewById(R.id.typeTxt);
            viewHolder.mPriceTxt = (TextView) convertView.findViewById(R.id.price);
            viewHolder.mNbProductTxt = (TextView) convertView.findViewById(R.id.nbProduct);

            viewHolder.mDelBtn = (Button) convertView.findViewById(R.id.delBtn);
            viewHolder.mAddBtn = (Button) convertView.findViewById(R.id.addBtn);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ItemPurchaseAdapter.ViewHolder) convertView.getTag();
        }

        if (product != null) {

            viewHolder.mIcon.setImageResource(product.getIconResource());
            viewHolder.mTypeTxt.setText(product.getName());
            String priceString = String.valueOf(product.getPrice());
            viewHolder.mPriceTxt.setText(priceString);

            viewHolder.mDelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnClickProductItem.removeOne((Button) v,viewHolder.mNbProductTxt,product);
                }
            });

            viewHolder.mAddBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnClickProductItem.addOne((Button) v,viewHolder.mNbProductTxt,product);
                }
            });
        }

        return convertView;
    }

    private static class ViewHolder {
        ImageView mIcon;
        TextView mTypeTxt;
        TextView mPriceTxt;
        TextView mNbProductTxt;

        Button mDelBtn;
        Button mAddBtn;
    }
}
