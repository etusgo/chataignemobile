package iut.projet.pointk.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import iut.projet.pointk.R;
import iut.projet.pointk.data.ItemHistory;

public class HistoryAdapter extends ArrayAdapter<ItemHistory> {

    private Context     mContext;
    private int         mListItemLayout;

    public HistoryAdapter(Context ctx, int layoutId, ArrayList<ItemHistory> itemHistory) {
        super(ctx, layoutId, itemHistory);

        this.mContext = ctx;
        this.mListItemLayout = layoutId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemHistory itemHistory = getItem(position);

        ViewHolder viewHolder;
        if(convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mListItemLayout, parent, false);

            viewHolder.mDate = (TextView) convertView.findViewById(R.id.tv_date);
            viewHolder.mCofeeQty = (TextView) convertView.findViewById(R.id.tv_qtyCoffee);
            viewHolder.mTeaQty = (TextView) convertView.findViewById(R.id.tv_qtyTea);
            viewHolder.mFruitQty = (TextView) convertView.findViewById(R.id.tv_qtyFruit);
            viewHolder.mCandyQty = (TextView) convertView.findViewById(R.id.tv_qtyCandy);
            viewHolder.mLittleCandyQty = (TextView) convertView.findViewById(R.id.tv_qtyLittleCandy);
            viewHolder.mPrice = (TextView) convertView.findViewById(R.id.tv_qtyChestnut);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (itemHistory != null) {
            viewHolder.mDate.setText(itemHistory.getDate());

            int coffeeQty = 0;
            int teaQty = 0;
            int fruitQty = 0;
            int candyQty = 0;
            int littleCandyQty = 0;

            for(int i=0 ; i<itemHistory.getProducts().size() ; i++) {
                switch(itemHistory.getProducts().get(i).getName()) {
                    case "cafe":
                        coffeeQty++;
                        break;
                    case "Thé":
                        teaQty++;
                        break;
                    case "Pomme bio":
                        fruitQty++;
                        break;
                    case "friandise":
                        candyQty++;
                        break;
                    case "mini-crepes":
                        littleCandyQty++;
                        break;
                }

                viewHolder.mCofeeQty.setText(String.valueOf(coffeeQty));
                viewHolder.mTeaQty.setText(String.valueOf(teaQty));
                viewHolder.mFruitQty.setText(String.valueOf(fruitQty));
                viewHolder.mCandyQty.setText(String.valueOf(candyQty));
                viewHolder.mLittleCandyQty.setText(String.valueOf(littleCandyQty));
                viewHolder.mPrice.setText(String.valueOf(itemHistory.getPrice()));
            }
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView mDate;
        TextView mCofeeQty;
        TextView mTeaQty;
        TextView mFruitQty;
        TextView mCandyQty;
        TextView mLittleCandyQty;
        TextView mPrice;
    }
}
