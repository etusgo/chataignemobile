package iut.projet.pointk.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import iut.projet.pointk.ExternalDB.ConnectMySql;
import iut.projet.pointk.InternalDB.DatabaseHelper;
import iut.projet.pointk.R;

public class LoginPage extends AppCompatActivity {

    private static final String     TAG = "LoginPage";

    private Button                  mConnectionBtn;
    private EditText                mLoginEditText;
    private EditText                mPasswordEditText;

    private DatabaseHelper mDBHelper;

    private View.OnClickListener    mBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.connexionBtn) {
                Log.d(TAG, "Connexion Clicked !");
                onClickConnexion();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initView();

        initInternDB();
    }

    private void onClickConnexion() {
        ConnectMySql.ConnectMySqlListener listener = new ConnectMySql.ConnectMySqlListener() {
            @Override
            public void onPostResult(Object[] result) {
                onGetResultConnexion(result);
            }

            @Override
            public void onProgress(Void... values) {

            }
        };
        ConnectMySql connectMySql = new ConnectMySql( ConnectMySql.Request.CONNEXION, listener);
        connectMySql.execute(mLoginEditText.getText().toString(),mPasswordEditText.getText().toString());
    }

    private void onGetResultConnexion(Object[] result) {
        Log.d("LoginPage","onGetREsult:result="+result);
        if (result != null){
            mDBHelper.registerAccount((Integer) result[0], (String) result[1], (String) result[2]);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void initInternDB() {
        mDBHelper=new DatabaseHelper(this.getApplicationContext());
    }

    private void initView() {
        setContentView(R.layout.activity_login_page);

        mConnectionBtn = findViewById(R.id.connexionBtn);
        mLoginEditText = findViewById(R.id.loginInput);
        mPasswordEditText = findViewById(R.id.passwordInput);

        mConnectionBtn.setOnClickListener(mBtnListener);
    }
}
