package iut.projet.pointk.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import iut.projet.pointk.ExternalDB.ConnectMySql;
import iut.projet.pointk.InternalDB.DatabaseHelper;
import iut.projet.pointk.R;
import iut.projet.pointk.data.Product;
import iut.projet.pointk.view.adapter.HistoryAdapter;
import iut.projet.pointk.view.adapter.ItemPurchaseAdapter;

import static iut.projet.pointk.data.Product.getAllProducts;

public class NewPurchase extends AppCompatActivity {

    private Button mConfirmBtn;
    private Button mCancelBtn;
    private TextView mUserLoginTxt;
    private TextView mTotalTxt;
    private DatabaseHelper mDBHelper;

    private ArrayList<Integer> chosenProduct = new ArrayList<>();

    public interface OnClickProductItem{
        void addOne(Button button, TextView numberProductTxt, Product product);
        void removeOne(Button button, TextView numberProductTxt, Product product);
    }

    private View.OnClickListener    mBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.cancelBtn) {
                finish();
            } else if(v.getId() == R.id.confirmBtn) {
                confirm();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initInternDB();

        initView();
    }

    private void initView() {
        setContentView(R.layout.activity_new_purchase);

        mTotalTxt = findViewById(R.id.total);

        mConfirmBtn = findViewById(R.id.confirmBtn);
        mCancelBtn = findViewById(R.id.cancelBtn);
        mUserLoginTxt = findViewById(R.id.userName);

        mCancelBtn.setOnClickListener(mBtnListener);
        mConfirmBtn.setOnClickListener(mBtnListener);
        mUserLoginTxt.setText(MainActivity.LOGIN);

        ListView scrollProductLinearLayout = findViewById(R.id.ScrollProductLinearLayout);

        OnClickProductItem onClickProductItem = new OnClickProductItem() {
            @Override
            public void addOne(Button button, TextView numberProductTxt, Product product) {
                chosenProduct.add(product.getId());
                updateConfirmButton();
                int nbItem = 0;
                for (int productId :
                        chosenProduct) {
                    if (product.getId() == productId){
                        nbItem++;
                    }
                }
                numberProductTxt.setText(nbItem+"");
                updatePrice();
            }

            @Override
            public void removeOne(Button button, TextView numberProductTxt, Product product) {
                if (chosenProduct.contains(product.getId())) {
                    if(chosenProduct.contains(product.getId())) {
                        chosenProduct.remove((Object)product.getId());
                    }
                    int nbItem = 0;
                    for (int productId :
                            chosenProduct) {
                        if (product.getId() == productId) {
                            nbItem++;
                        }
                    }
                    numberProductTxt.setText(nbItem + "");
                    updateConfirmButton();
                    updatePrice();
                }
            }
        };

        ItemPurchaseAdapter purchaseAdapter = new ItemPurchaseAdapter(this, R.layout.item_purchase, getAllProducts(), onClickProductItem);
        scrollProductLinearLayout.setAdapter(purchaseAdapter);

    }

    private void updatePrice() {
        float total = 0;
        for (int productId : chosenProduct) {
            total += Product.getProductById(productId).getPrice();
        }
        mTotalTxt.setText(String.valueOf(total));
    }

    private void initInternDB() {
        mDBHelper=new DatabaseHelper(this.getApplicationContext());
    }

    private void updateConfirmButton() {
        mConfirmBtn.setEnabled(chosenProduct.size()!=0);
    }

    private void confirm() {
        ConnectMySql.ConnectMySqlListener listener = new ConnectMySql.ConnectMySqlListener() {
            @Override
            public void onPostResult(Object[] result) {
                onGetResultBuy(result);
            }

            @Override
            public void onProgress(Void... values) {

            }
        };
        ConnectMySql connectMySql = new ConnectMySql( ConnectMySql.Request.BUY, listener);
        String[] sendData = new String[chosenProduct.size()+2];

        sendData[0] = String.valueOf(MainActivity.userID);
        sendData[1] = mTotalTxt.getText().toString();

        int i = 2;
        for ( Integer product_id:chosenProduct) {
            sendData[i] = String.valueOf(product_id);
            i++;
        }
        connectMySql.execute(sendData);
    }

    private void onGetResultBuy(Object[] result) {
        //TODO
        this.finish();
    }
}