package iut.projet.pointk.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import iut.projet.pointk.ExternalDB.ConnectMySql;
import iut.projet.pointk.InternalDB.DatabaseHelper;
import iut.projet.pointk.R;
import iut.projet.pointk.data.ItemHistory;

import iut.projet.pointk.view.adapter.HistoryAdapter;

public class HistoryActivity extends AppCompatActivity {

    private ListView mListHistory;
    private ArrayList<ItemHistory> mItemHistory = new ArrayList<>();
    private ImageView mLogout;
    private Button mNewPurchase;
    private TextView mUserLogin;
    private TextView mSole;
    private DatabaseHelper mDBHelper;
    private Context mContext;

    private View.OnClickListener    mBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.btn_logout) {
                Intent intent = new Intent(mContext, LoginPage.class);
                startActivity(intent);
                mDBHelper.disconnectAccount();
            } else if(v.getId() == R.id.btn_new_purchase) {
                Intent intent = new Intent(mContext, NewPurchase.class);
                startActivity(intent);
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        getItemHistory();
        getSole();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        mContext = this;
        mDBHelper = new DatabaseHelper(this);

        mLogout = (ImageView) findViewById(R.id.btn_logout);
        mNewPurchase = (Button) findViewById(R.id.btn_new_purchase);
        mUserLogin = (TextView) findViewById(R.id.userName);
        mSole = (TextView) findViewById(R.id.sole);

        mNewPurchase.setOnClickListener(mBtnListener);
        mLogout.setOnClickListener(mBtnListener);

        mUserLogin.setText(MainActivity.LOGIN);
    }

    private void getSole() {
        ConnectMySql.ConnectMySqlListener listener = new ConnectMySql.ConnectMySqlListener() {
            @Override
            public void onPostResult(Object[] result) {
                onGetResultSole(result);
            }

            @Override
            public void onProgress(Void... values) {

            }
        };

        ConnectMySql connectMySql = new ConnectMySql( ConnectMySql.Request.BALANCE, listener);
        connectMySql.execute(String.valueOf(MainActivity.userID));
    }

    private void onGetResultSole(Object[] result) {
        if(result != null) {
            mSole.setText(String.valueOf((float) result[0]));
        }
    }

    private void getItemHistory() {
        ConnectMySql.ConnectMySqlListener listener = new ConnectMySql.ConnectMySqlListener() {
            @Override
            public void onPostResult(Object[] result) {
                onGetResultItemHistory(result);
            }

            @Override
            public void onProgress(Void... values) {

            }
        };

        ConnectMySql connectMySql = new ConnectMySql( ConnectMySql.Request.HISTORY, listener);
        connectMySql.execute(String.valueOf(MainActivity.userID));
    }

    private void onGetResultItemHistory(Object[] result) {
        mItemHistory = new ArrayList<>();
        for (Object o : result) {
            mItemHistory.add((ItemHistory) o);
        }

        HistoryAdapter historyAdapter = new HistoryAdapter(this, R.layout.item_history, mItemHistory);
        mListHistory = (ListView) findViewById(R.id.listViewHistory);
        mListHistory.setAdapter(historyAdapter);
    }
}