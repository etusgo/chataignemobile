package iut.projet.pointk.data;

import android.content.ClipData;

import java.sql.Date;
import java.util.ArrayList;

public class ItemHistory {

    private String mDate;
    private ArrayList<Product> mProducts;
    private float  mPrice;

    public ItemHistory(String date, ArrayList<Product> products) {
        this.mDate = date;
        this.mProducts = products;

        calculatePrice();
    }

    public ItemHistory(int idProduct, Date date) {
        this.mDate = date.toString();

        mProducts = new ArrayList<Product>();
        Product firstProduct = Product.getProductById( idProduct);
        mProducts.add(firstProduct);

        calculatePrice();
    }

    public void addProductWithId(int idProduct ){
        Product firstProduct = Product.getProductById( idProduct);
        mProducts.add(firstProduct);

        calculatePrice();
    }

    public String getDate() {
        return mDate;
    }

    public ArrayList<Product> getProducts() {
        return mProducts;
    }

    public float getPrice() {
        return mPrice;
    }

    private void calculatePrice() {
        this.mPrice = 0;
        for(int i=0 ; i<mProducts.size() ; i++) {
            this.mPrice += mProducts.get(i).getPrice();
        }
    }
}
