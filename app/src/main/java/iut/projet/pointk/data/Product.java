package iut.projet.pointk.data;

import android.bluetooth.le.ScanFilter;

import java.util.ArrayList;

import iut.projet.pointk.R;

public class Product {

    private static ArrayList<Product> allProducts;

    public static void setAllProducts(ArrayList<Product> products){

        allProducts=products;
    }

    public static ArrayList<Product> getAllProducts(){

        return allProducts;

    }

    public static Product getProductById(int idProduct) {
        for (Product product :
                allProducts) {
            if (product.getId() == idProduct) return product;
        }
        return null;
    }

    private int id;
    private String name;
    private int iconResource;
    private float price;

    public Product(int id, String name, int iconResource, float price) {
        this.id=id;
        this.name = name;
        this.iconResource = iconResource;
        this.price = price;
    }

    public Product(int id, String name, float price) {
        this.id = id;
        this.name = name;
        this.price = price;
        //TODO set ressource drawable
        switch (id){
            case 1 :
                this.iconResource = R.drawable.ic_tea;
                break;
            case 2 :
                this.iconResource = R.drawable.ic_candy;
                break;
            case 3 :
                this.iconResource = R.drawable.ic_coffee;
                break;
            case 4 :
                this.iconResource = R.drawable.ic_small_candy;
                break;
            case 5:
                this.iconResource = R.drawable.ic_fruit;
                break;
            default : this.iconResource = R.drawable.ic_breakfast;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIconResource() {
        return iconResource;
    }

    public void setIconResource(int iconResource) {
        this.iconResource = iconResource;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
