package iut.projet.pointk.InternalDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.UUID;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="CHATEIGNE";

    private static final String TABLE_NAME_COMPTE = "COMPTE";
    private static final String COLUMN_COMPTE_LOGIN = "login";
    private static final String COLUMN_COMPTE_PASSWORD = "password";
    private static final String COLUMN_ID = "id";
    private static final String CREATE_TABLE_COMPTE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_COMPTE + "( " + COLUMN_ID + " INT," + COLUMN_COMPTE_LOGIN + " TEXT, " + COLUMN_COMPTE_PASSWORD + " TEXT)";
    private static final String DELETE_TABLE_COMPTE = "DROP TABLE IF EXISTS " + TABLE_NAME_COMPTE;

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_COMPTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DELETE_TABLE_COMPTE);

        onCreate(db);
    }

    public void registerAccount(int id, String login, String password){
        SQLiteDatabase database=this.getReadableDatabase();

        ContentValues values=new ContentValues();
        values.put(COLUMN_ID, id);
        values.put(COLUMN_COMPTE_LOGIN, login);
        values.put(COLUMN_COMPTE_PASSWORD, password);

        //TODO change to reset table
        database.execSQL(DELETE_TABLE_COMPTE);
        database.execSQL(CREATE_TABLE_COMPTE);

        database.insert(TABLE_NAME_COMPTE,null,values);

        database.close();
    }

    public void disconnectAccount(){
        SQLiteDatabase database=this.getReadableDatabase();

        //TODO change to reset table
        database.execSQL(DELETE_TABLE_COMPTE);
        database.execSQL(CREATE_TABLE_COMPTE);

        database.close();
    }

    public Object[] getAccount(){
        String selectQuery= "SELECT * FROM " + TABLE_NAME_COMPTE;
        SQLiteDatabase database=this.getReadableDatabase();
        Cursor cursor=database.rawQuery(selectQuery,null);

        String login, password;
        int id;
        if (cursor.moveToFirst()){
            id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
            login = cursor.getString(cursor.getColumnIndex(COLUMN_COMPTE_LOGIN));
            password = cursor.getString(cursor.getColumnIndex(COLUMN_COMPTE_PASSWORD));
            database.close();
            return new Object[]{id,login, password};
        }

        return null;
    }
}
