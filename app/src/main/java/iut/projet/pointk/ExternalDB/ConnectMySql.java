package iut.projet.pointk.ExternalDB;

import android.os.AsyncTask;
import android.util.Log;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;

import iut.projet.pointk.data.ItemHistory;
import iut.projet.pointk.data.Product;

public class ConnectMySql extends AsyncTask<String, Void, Object[]> {

    private static final String user = "annuaire";
    private static final String pass = "SuperPandasTeam91!";
    private static final String url = "jdbc:mariadb://www5.geeps.centralesupelec.fr:3306/annuaire?user="+user+"&password="+pass;

    ConnectMySqlListener mListener;
    Request mRequest;

    public enum Request{
        CONNEXION,
        BUY,
        HISTORY,
        BALANCE,
        ALL_PRODUCT_TYPE
    }

    public interface ConnectMySqlListener{
        void onPostResult(Object[] result);
        void onProgress(Void... values);
    }

    public ConnectMySql(Request request, ConnectMySqlListener connectMySqlListener){
        mListener=connectMySqlListener;
        mRequest=request;

    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Object[] doInBackground(String... params) {
        Object[] res=null;
        try {
            Class.forName("org.mariadb.jdbc.Driver").newInstance();
            Connection con = DriverManager.getConnection(url);
            System.out.println("Databaseection success");

            //String result = "Database Connection Successful\n";
            Statement st = con.createStatement();
            ResultSet rs=null;
            switch (mRequest) {

                case CONNEXION:
                    rs = st.executeQuery("select * from compte where login='"+params[0]+"' AND password='"+params[1]+"';");

                    if (rs.next()) {//replace to while for more result

                        int id = rs.getInt("id");
                        String login = rs.getString("login");
                        String password = rs.getString("password");

                        res = new Object[]{id,login, password};
                    } else return null;
                    break;

                case ALL_PRODUCT_TYPE:
                    rs = st.executeQuery("select * from pointk_produit;");
                    ArrayList<Product> products = new ArrayList<Product>();
                    while (rs.next()) {//replace to while for more result

                        int id = rs.getInt("id");
                        String name = rs.getString("nom");
                        float nbChateigne = rs.getFloat("nb_chataigne");

                        res = new Object[]{id, name, nbChateigne};
                        products.add(new Product(id,name,nbChateigne));
                    }
                    res = products.toArray();

                    break;

                case BUY:

                    for (int i = 2; i<params.length ; i++)
                        rs = st.executeQuery("insert into pointk_achat (id_produit, id_compte) values ("+params[i]+"," + params[0] + ");");
                        rs = st.executeQuery("select solde_chataigne from pointk_solde where  id_proprietaire=" + params[0] + ";");

                        if ( rs.next() ) {
                            float nbChateigne = rs.getFloat("solde_chataigne");
                            nbChateigne -= Float.valueOf(params[1]);
                            rs = st.executeQuery("update pointk_solde set solde_chataigne=" + nbChateigne + "where id_proprietaire=" + params[0] + ";");
                        }

                    break;

                case BALANCE:
                    rs = st.executeQuery("select solde_chataigne from pointk_solde where id_proprietaire=" + params[0] + ";");
                    if (rs.next()) {//replace to while for more result

                        float solde = rs.getFloat("solde_chataigne");

                        res = new Object[]{solde};
                    } else return null;
                    break;

                case HISTORY:
                    rs = st.executeQuery("select * from pointk_achat where id_compte=" + params[0] + " order by date DESC;");

                    ArrayList<ItemHistory> itemHistories = new ArrayList<ItemHistory>();
                    Date currentDate = null;
                    ItemHistory currentItemHistory = null;

                    if ( rs.next() ){
                        int idProduct = rs.getInt("id_produit");
                        currentDate = rs.getDate("date");
                        currentItemHistory = new ItemHistory(idProduct,currentDate);
                        itemHistories.add(currentItemHistory);
                    }

                    while (rs.next()) {
                        int idProduct = rs.getInt("id_produit");
                        Date date = rs.getDate("date");

                        assert currentDate != null;
                        if (Math.abs(date.getTime()-currentDate.getTime())<300000){
                            currentItemHistory.addProductWithId(idProduct);
                        }
                        else {
                            currentDate = date;
                            currentItemHistory = new ItemHistory(idProduct,date);
                            itemHistories.add(currentItemHistory);
                        }
                    }

                    res = itemHistories.toArray();
                    break;

                default: return null;
            }

            con.close();

            //ResultSetMetaData rsmd = rs.getMetaData();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        mListener.onProgress(values);
    }

    @Override
    protected void onPostExecute(Object[] result) {
        mListener.onPostResult(result);
    }

}
